# C-Math [![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://opensource.org/licenses/AGPL-3.0/) [![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.png?v=103)](https://opensource.com/resources/what-open-source)

### Build Status

Not available

---

### Quality Review

[![CodeFactor](https://www.codefactor.io/repository/github/mahdibaghbani/c-math/badge)](https://www.codefactor.io/repository/github/mahdibaghbani/c-math)

---

Mathematical methods implemented in C language.
